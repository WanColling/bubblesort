# README #

This README would normally document whatever steps are necessary to get your application up and running.

### BubbleSort ###

* This is one of easy code in c++ programming for bubble sort to sort numbers or arrange them in ascending order. You can easily modify it to print numbers in descending order.


Wan Colling - web developer at http://applianceauthority.org/coffee-makers/
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

![bubble-sort-c.png](https://bitbucket.org/repo/j8jpLb/images/3816908336-bubble-sort-c.png)!
### /* Bubble sort code */ ###
 
#include <stdio.h>
 
int main()
{
  int array[100], n, c, d, swap;
 
  printf("Enter number of elements\n");
  scanf("%d", &n);
 
  printf("Enter %d integers\n", n);
 
  for (c = 0; c < n; c++)
    scanf("%d", &array[c]);
 
  for (c = 0 ; c < ( n - 1 ); c++)
  {
    for (d = 0 ; d < n - c - 1; d++)
    {
      if (array[d] > array[d+1]) /* For decreasing order use < */
      {
        swap       = array[d];
        array[d]   = array[d+1];
        array[d+1] = swap;
      }
    }
  }
 
  printf("Sorted list in ascending order:\n");
 
  for ( c = 0 ; c < n ; c++ )
     printf("%d\n", array[c]);
 
  return 0;
}